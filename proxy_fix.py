class ReverseProxyFix(object):
    '''Fix flask's request handling to account for a Reverse Proxy

    Allows the app to run in a container on port 80/443, as far as it knows, with any URL prefix, while it's really
    accessed by users with an arbitrary hostname and port. REQUIRES custom headers to be sent by the webserver.

    Includes fixes:
        - werkzeug.contrib.fixers ProxyFix to get the right values for HTTP_HOST and REMOTE_ADDR
          (https://github.com/pallets/werkzeug/blob/6bb8e496a19d9581e0d1cbedb5633611cd9dda40/werkzeug/contrib/fixers.py#L97)
        - SCRIPT_NAME snippet to fix SCRIPT_NAME and PATH_INFO behind a reverse proxy
          (http://flask.pocoo.org/snippets/35/)
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        forwarded_proto = environ.get('HTTP_X_FORWARDED_PROTO', '')
        forwarded_for = environ.get('HTTP_X_FORWARDED_FOR', '').split(',')[0]
        forwarded_host = environ.get('HTTP_X_FORWARDED_HOST', '')
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')

        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]
        if forwarded_for is not None:
            environ['REMOTE_ADDR'] = forwarded_for
        if forwarded_host:
            environ['HTTP_HOST'] = forwarded_host
        if forwarded_proto:
            environ['wsgi.url_scheme'] = forwarded_proto
        return self.app(environ, start_response)
