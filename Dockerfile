FROM python:3.6-alpine

RUN pip install pipenv

WORKDIR /app

COPY . .
RUN pipenv install --system
