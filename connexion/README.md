Hello World Example
===================

## License

Hello World example obtained from https://github.com/zalando/connexion

Changes: Applied custom Proxy Fix (ex. http://flask.pocoo.org/snippets/35/) to test if SwaggerUI then works behind a proxy

Connexion - Copyright 2015 Zalando SE

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
