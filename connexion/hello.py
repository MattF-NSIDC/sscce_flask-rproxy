#!/usr/bin/env python3

import connexion

from proxy_fix import ReverseProxyFix

def post_greeting(name: str) -> str:
    return 'Hello {name}'.format(name=name)

if __name__ == '__main__':
    app = connexion.FlaskApp(__name__, host='0.0.0.0', port=9090, specification_dir='swagger/')
    print(dir(app.app))
    app.app.wsgi_app = ReverseProxyFix(app.app.wsgi_app)
    app.add_api('helloworld-api.yaml', arguments={'title': 'Hello World Example'})
    app.run()
