from flask import Flask, request
from flask_restplus import Resource, Api

from proxy_fix import ReverseProxyFix


app = Flask(__name__)
app.wsgi_app = ReverseProxyFix(app.wsgi_app)
api = Api(app)


@api.route('/test')
class TodoSimple(Resource):
    def get(self):
        return {'hello': 'world'}


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
