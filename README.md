To test the fix for running a flask app and swagger UI behind a proxy with an arbitrary UI address and URI prefix.

# Running the application

    docker-compose up

# Testing

Connexion is running under `host:1951/cx` and flask-restplus is at `host:1951/rp`.

## connexion

To test connexion, go to `host:1951/cx/v1.0/ui/`. Expected result:

    Can't read swagger JSON from host:1951/v1.0/swagger.json

Note that the URL lacks the `/cx` location prefix.

Modify the text field at the top of the page from `host:1951/v1.0/swagger.json` to `host:1951/cx/v1.0/swagger.json` where `host` is the hostname you're running the stack on. Submit, and you should get a UI that, at first glance, appears to be working.

Note the `BASE URL` value in the footer is `/v1.0`. When you attempt to use the `Try it out!` feature, the request URL will not include the `cx/` prefix and the response will be a 404.

## flask-restplus

To test flask-restplus, visit `host:1951/rp/test`. Expected result: working Swagger UI. Note the `BASE URL` value in the footer is `/rp`.
